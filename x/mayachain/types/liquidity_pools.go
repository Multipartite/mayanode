//go:build !mocknet && !stagenet
// +build !mocknet,!stagenet

package types

import (
	"gitlab.com/mayachain/mayanode/common"
)

var LiquidityPools = common.Assets{
	common.BTCAsset,
	common.BNBAsset,
	common.ETHAsset,
}
