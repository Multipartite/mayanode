package types

import (
	"errors"
	"fmt"

	"github.com/cosmos/cosmos-sdk/codec"

	"gitlab.com/mayachain/mayanode/common"
	"gitlab.com/mayachain/mayanode/common/cosmos"
)

var _ codec.ProtoMarshaler = &LiquidityProvider{}

// LiquidityProviders a list of liquidity providers
type LiquidityProviders []LiquidityProvider

// Valid check whether lp represent valid information
func (m *LiquidityProvider) Valid() error {
	if m.LastAddHeight == 0 {
		return errors.New("last add liquidity height cannot be empty")
	}
	if m.AssetAddress.IsEmpty() && m.CacaoAddress.IsEmpty() {
		return errors.New("asset address and rune address cannot be empty")
	}
	return nil
}

func (lp LiquidityProvider) GetAddress() common.Address {
	if !lp.CacaoAddress.IsEmpty() {
		return lp.CacaoAddress
	}
	return lp.AssetAddress
}

// Key return a string which can be used to identify lp
func (lp LiquidityProvider) Key() string {
	return fmt.Sprintf("%s/%s", lp.Asset.String(), lp.GetAddress().String())
}

func (lp LiquidityProvider) IsLiquidityBondProvider() bool {
	return !lp.NodeBondAddress.Empty()
}

// SetNodeAccount sets the nodeAccount to which the LPs have provided liquidity bond
func (lps LiquidityProviders) SetNodeAccount(na cosmos.AccAddress) {
	for i := range lps {
		lps[i].NodeBondAddress = na
	}
}
