//go:build stagenet
// +build stagenet

package types

import (
	"gitlab.com/mayachain/mayanode/common"
)

var LiquidityPools = common.Assets{
	common.BTCAsset,
	common.BNBAsset,
	common.ETHAsset,
}
